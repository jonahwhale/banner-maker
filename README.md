# README #

* Help with turning a large amount of heterogeneous images into a large scale printed banner. 
* Analyze images in a folder and create square thumbs of various target sizes.

# INSTALLATION #
This script relies on the 'identify' and 'convert' commands.  To install ImageMagick on OS X, do, from Terminal:

'brew install imagemagick'

To run this script, put it into a directory with a lot of jpgs.  Then, from terminal, cd into the directory and run:

'php imageStat.php'

If the assumptions aren't right, open the file and change the variables.  When you are ready to make the output images use:

'php imageStat.php make'
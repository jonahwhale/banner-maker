<?php
/**
* Help with turning a large amount of heterogeneous images into a large scale printed banner. 
* Analyze images in a folder and create square thumbs of various target sizes.
*
* @liscense MIT
* @version 1.0 Jonah B jonahb0001@gmail.com  4/29/15 10:26 AM
*/

$im = new imager();
$im->summary();
$im->px();

class imager {
    
    var $imageTargetSizeInches = 6;
    var $minWidthInches = 2;
    var $targetRezPerInch = 120;
    var $sizes = array("8","6","4","2");
    var $makeOutput = false;
    var $imageDataCmd = "identify *.jpg";
    var $mode = "identify"; // identify or sips.  
    
    function summary() {
        $this->minWidth = $this->targetRezPerInch*$this->minWidthInches;
        if($this->mode=="sips") {
            $this->imageDataCmd = "sips -g pixelHeight -g pixelWidth *.jpg";
        }
        echo("\n== TARGETS == ");
        echo("\n*            Target banner size: 10 feet x 50 feet, ".$this->targetRezPerInch."ppi");
        echo("\n* Possible image sizes (square): ".implode(",",$this->sizes));
        $this->targetWidth = 10*12*$this->targetRezPerInch;
        echo("\n*            Target Pixel Width: ".$this->targetWidth);

        $this->targetHeight = 50*12*$this->targetRezPerInch;
        echo("\n*           Target Pixel Height: ".$this->targetHeight);
        $this->targetTotal = $this->targetWidth * $this->targetHeight;
        echo("\n*           Target Total Pixels: ".$this->targetTotal);
        $this->squareFeet = 500;
        echo("\n*            Target Square Feet: ".$this->squareFeet);
        echo("\n*   Target Banner Square Inches: ".$this->squareFeet*(12*12));
        
        
        echo("\n*   Target image size in inches: ".$this->imageTargetSizeInches);
        
        $this->imageTargetSizeWidth = $this->imageTargetSizeInches * $this->targetRezPerInch;
        echo("\n*     Target min image px width: ".$this->imageTargetSizeWidth);

        
        $this->imagesPerSquareFoot = 12/$this->imageTargetSizeInches;
        $this->imagesPerSquareFoot = $this->imagesPerSquareFoot * $this->imagesPerSquareFoot;
        echo("\n* Target images per square foot: ".$this->imagesPerSquareFoot);
        
        $this->targetImageCount = $this->imagesPerSquareFoot*$this->squareFeet;
        echo("\n*           Target total images: ".$this->targetImageCount);
        
        echo("\n*           Minimum image width: ".$this->minWidth);
        
        
    }
    /**
    * @version 1.0 Jonah B  4/28/15 4:10 PM
    */
    function px() {
        global $argv;
        if($argv[1]=="make") {
            $this->makeOutput = true;
        }
        $ls = $this->run($this->imageDataCmd);
        /*
        if(strpos($ls,"command not found")) {
            $ls = $this->run("identify *.jpg");
            // print_r($ls);
        }
        */
        // print_r($ls);
        echo("\n== BATCH DATA ==");
        
        if($this->mode=="identify") {
            foreach($ls AS $current=>$l) {
                unset($info);
                $disqualified = false;
                $info = $this->parseIdentify($l);
                extract($info);
                if($h<$this->minWidth || $w<$this->minWidth) {
                    $this->disqualifiedCount++;
                    continue;
                } else {                
                    $this->height[] = $h; 
                    $this->heightTotal = $this->heightTotal + $h;
                    
                    $this->width[] = $w; 
                    $this->widthTotal = $this->widthTotal + $w;
                    $file = trim($ls[$current-1]);
                    $this->files[] = $info['file'];
                    $this->imageCount++;
                    // print_r($info['file']);
                    $this->analyzeImage($info['file'],$w,$h);
                } 
                
            }        
        } 
        // print_r($this->height);
        echo("\n   Total pixel height: ".$this->heightTotal);
        // print_r($this->width);
        echo("\n    Total pixel width: ".$this->widthTotal);
        echo("\n         Total Pixels: ".$this->widthTotal*$this->heightTotal);
        echo("\n          Image Count: ".$this->imageCount);
        echo("\n  Disqualified Images: ".$this->disqualifiedCount);
        
        if(is_array($this->countBySize)) {
            ksort($this->countBySize);
            foreach($this->countBySize AS $size=>$count) {
                echo("\n        ".$size." inch images: ".$count);
                $this->squareInches[$size] = $count*($size*$size);
                echo("\twith ".$this->squareInches[$size]." square inches");
                $this->totalSquareInches = $this->totalSquareInches + $this->squareInches[$size];
            }
        }
        
        echo("\n   Batch Total square inches: ".$this->totalSquareInches);
        echo("\n Target Banner Square Inches: ".$this->squareFeet*(12*12));

        print_r("\n");
    }
    
    /**
    * Legacy
    *
    * @version 1.0 Jonah B  4/30/15 12:02 PM
    */
    function processSips($ls) {
        foreach($ls AS $current=>$l) {
            $disqualified = false;
            if(strpos($l,"Height:")) {
                list($j,$h) = explode(":",$l);
                list($j,$w) = explode(":",$ls[$current+1]);
                $h = trim($h);
                $w = trim($w);
                if($h<$this->minWidth || $w<$this->minWidth) {
                    $this->disqualifiedCount++;
                    continue;
                } else {                
                    $this->height[] = $h; 
                    $this->heightTotal = $this->heightTotal + $h;
                    
                    $this->width[] = $w; 
                    $this->widthTotal = $this->widthTotal + $w;
                    $file = trim($ls[$current-1]);
                    $this->files[] = $file;
                    $this->imageCount++;
                    $this->analyzeImage($file,$w,$h);
                }
            }   
            
        }
         
    }
    
    /**
    * @version 1.0 Jonah B  4/30/15 11:38 AM
    */
    function parseIdentify($in) {
        $debug = false;
        //if($debug) print_r($in);
        $parts = explode(" ",$in);
        //if($debug) print_r($parts);
        $out['file'] = $parts[0];
        if(strpos($out['file'],"[")) {
            list($out['file'],$junk) = explode("[",$out['file']);
        }
        $out['file_type'] = $parts[1];
        $wh = $parts[2];
        list($out['w'],$out['h']) = explode("x",$wh);
        if($debug) print_r($out);
        return $out;
    }
    /**
    * @version 1.0 Jonah B  4/29/15 8:15 AM
    */
    function analyzeImage($filePath,$w='',$h='') {
        if(!$w) {
            $info = $this->run("identify ".$filePath);
            // 2465356_348_1826700_user_action.jpg JPEG 500x281 500x281+0+0 8-bit DirectClass 33.4kb
            $parts = explode(" ",$info);
            $file_name = $parts[0];
            $file_type = $parts[1];
            $wh = $parts[2];
            list($w,$h) = explode($wh);
            
        }
        
        foreach($this->sizes AS $size) {
            if(!file_exists("./out/")) {
                mkdir("./out/");
            }
            $outPath = "out/".$size."/";
            if(!file_exists($outPath)) {
                mkdir($outPath);
            }
            $minPixels = $size * $this->targetRezPerInch;
            if($w>=$minPixels && $h>=$minPixels) {
                $this->countBySize[$size]++;
                $cmd = "convert ".$filePath." -resize ".$minPixels."x".$minPixels."^ -gravity Center -crop ".$minPixels."x".$minPixels."+0+0 +repage";
                $cmd .= " ".$outPath."".$this->countBySize[$size].".png";
                // echo("\n* ".$cmd);
                if($this->makeOutput) {
                    $res = $this->run($cmd);
                }
                break;
            }
        }
    }
    
    function run($cmd) {
        ob_start();
        passthru($cmd);
        $out = ob_get_clean();
        $out = trim($out);
        $out = explode("\n",$out);
        return $out;
    }
}

?>